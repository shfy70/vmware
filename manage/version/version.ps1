param($username,$passwd)
$vcenter = 'vc.ysftek.lab'
Set-PowerCLIConfiguration -InvalidCertificateAction Ignore -Confirm:$False
Connect-VIServer -Server $vcenter -User $username -Password $passwd -ErrorAction Stop 
 
$dc = Get-Datacenter
$cluster = $dc | Get-Cluster  
$report = @()
$vmhosts = $cluster | Get-VMhost
foreach ($vmhost in $vmhosts) 
	{
        $row = "" | Select-Object VC,datacenter,cluster,VMHost,esxversion,build
        $row.vc = $vcenter
        $row.datacenter = $dc.name
        $row.cluster = $cluster.Name                     
        $row.esxversion = $vmhost.version
        $row.build =  $vmhost.build
        $row.VMHost = $vmhost.Name                                         
        $report += $row
    }    


Try {Disconnect-VIServer * -Force -Confirm:$false}
    Catch {}


$date = get-date -Format  yyyy-MM-dd-HH-mm
$Filename =  'ESXi Version Report' + $date + '.csv'
$report | Sort-Object VC,datacenter,cluster,VMHost,esxversion,build | Export-Csv $Filename -NoTypeInformation
Copy-Item $Filename "import.csv"

$PSEmailServer = "smtp.gmail.com"
$SMTPPort = 587
$SMTPUsername = "norman.yang@hellofresh.com"
#On jenkins server, In powershell: Read-Host -AsSecureString | ConvertFrom-SecureString | Out-File securepass #type after prompt, save as $EncryptedPassword
#$EncryptedPassword = '6900720065007400670078007a006d006200720072006500680066007a006f00' #gmail app password
$SecureStringPassword = ConvertTo-SecureString -String "iqwu jyvv usxc sads" -AsPlainText -Force #$EncryptedPassword | ConvertTo-SecureString
$EmailCredential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $SMTPUsername,$SecureStringPassword
$MailTo = "ysfbuy@gmail.com"
$MailFrom = "norman.yang@hellofresh.com"
$MailSubject = (get-date -Format yyyy-MM-dd-HH-mm)+ " "+ "ESXi Version report"
$MailBody = "$date ESXi Version Report"
Send-MailMessage -To $MailTo -From $MailFrom -Subject $MailSubject -Body $MailBody -SmtpServer $PSEmailServer -Port $SMTPPort -Credential $EmailCredential -Attachments $Filename -UseSSL


