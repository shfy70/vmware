param($username,$passwd)
$vcenter = 'vc.ysftek.lab'
Set-PowerCLIConfiguration -InvalidCertificateAction Ignore -Confirm:$False
Connect-VIServer -Server $vcenter -User $username -Password $passwd -ErrorAction Stop 
 
$dc = Get-Datacenter
$cluster = $dc | Get-Cluster  
$report = @()
$vmhosts = $cluster | Get-VMhost
foreach ($vmhost in $vmhosts) 
	{
        Write-Host "checking $($vmhost.name)"
        $vkernel_ipadrees = get-vmhost $vmhost | Get-VMHostNetworkAdapter -VMKernel | Where-Object {$_.VMotionEnabled -eq “True”}
        $info = '' | Select-Object Datacenter, Cluster, Server, vMotion_IP
        $info.Datacenter = $dc.Name
        $info.Cluster = $cluster.Name
        $info.Server = $vmhost.name 
        $info.vMotion_IP = $vkernel_ipadrees.ip
        $report += $info
    }    


Try {Disconnect-VIServer * -Force -Confirm:$false}
    Catch {}


$date = get-date -Format  yyyy-MM-dd-HH-mm
$Filename =  'ESXi vMotion IP Report ' + $date + '.csv'
$report | Sort-Object Datacenter, Cluster, Server, vMotion_IP | Export-Csv $Filename -NoTypeInformation
Copy-Item $Filename "import.csv"

$PSEmailServer = "smtp.gmail.com"
$SMTPPort = 587
$SMTPUsername = "ysftek75@gmail.com"
#On jenkins server, In powershell: Read-Host -AsSecureString | ConvertFrom-SecureString | Out-File securepass #type after prompt, save as $EncryptedPassword
$EncryptedPassword = '6900720065007400670078007a006d006200720072006500680066007a006f00' #gmail app password
$SecureStringPassword = $EncryptedPassword | ConvertTo-SecureString
$EmailCredential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $SMTPUsername,$SecureStringPassword
$MailTo = "ysfbuy@gmail.com"
$MailFrom = "ysftek75@gmail.com"
$MailSubject = (get-date -Format yyyy-MM-dd-HH-mm)+ " "+ "ESXi vMotion IP Report "
$MailBody = "$date ESXi Version Report"
Send-MailMessage -From $MailFrom -To $MailTo -Subject $MailSubject -Body $MailBody -SmtpServer $PSEmailServer -Port $SMTPPort -Credential $EmailCredential -Attachments $Filename -UseSsl

